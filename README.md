# CryptoCurrency

What does this app do:

- fetch the exchange rates for all cryptocurrencies in USD and display them as a list with handling the pagination. 
- User can compare between two or more cryptocurrencies through dynamic bar charts. So that the user can add more currencies from the cryptocurrencies list or remove from the bar chart module.
- User can calculate the amount of dollars equivalent to any cryptocurrency through simple calculator so that he can choose the cryptocurrency he wishes and write down the amount he wants to convert in USD.
- All the data should be refreshed every 5 minutes.


This app Designed with MVVM architecure design pattern

Use android architecture components

- LiveData    
- Paging Library
- ViewModel

use Custom BarChart 



