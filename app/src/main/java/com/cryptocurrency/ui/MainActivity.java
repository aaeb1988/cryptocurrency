package com.cryptocurrency.ui;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.cryptocurrency.R;
import com.cryptocurrency.model.CheckBoxListener;
import com.cryptocurrency.model.CurrencyAdapter;
import com.cryptocurrency.model.ListItemClickListener;
import com.cryptocurrency.model.POJO.AllCurrencies;
import com.cryptocurrency.model.POJO.CurrencyModel;
import com.cryptocurrency.model.POJO.CurrencyValue;
import com.cryptocurrency.view$model.AllCurrenciesViewModel;
import com.cryptocurrency.view$model.ConvertViewModel;
import com.cryptocurrency.view$model.CurrenciesViewModel;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class MainActivity extends AppCompatActivity implements ListItemClickListener {

    private CurrenciesViewModel currenciesViewModel;
    private ConvertViewModel convertViewModel;
    private AllCurrenciesViewModel allCurrenciesViewModel;

    private RecyclerView recyclerView;
    private Unbinder unbinder;
    private RecyclerPopupWindow recyclerPopupWindow;
    List<AllCurrencies.Datum> items;


    @BindView(R.id.ed_amount)
    EditText Edit_Amount;

    @BindView(R.id.tv_item)
    TextView TV_Item;

    @BindView(R.id.tv_result)
    TextView TV_Result;

    @BindView(R.id.tv_compare)
    TextView TV_Compare;

    @BindView(R.id.swipe)
    SwipeRefreshLayout swipeRefreshLayout;

    private AllCurrencies.Datum currentItem = null;
    private ArrayList<CurrencyModel.Datum> mCheckedItems = new ArrayList<>();

    private CheckBoxListener listener = new CheckBoxListener() {
        @Override
        public void onItemChecked(CurrencyModel.Datum datum, boolean isChecked) {
            if (isChecked) {
                mCheckedItems.add(datum);
            } else {
                mCheckedItems.remove(datum);
            }

            if (mCheckedItems.size() > 0) {
                TV_Compare.setVisibility(View.VISIBLE);
            } else {
                TV_Compare.setVisibility(View.GONE);
            }


        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        unbinder = ButterKnife.bind(this);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(false);
            }
        });
        recyclerView = findViewById(R.id.currency_recycler);
        LinearLayoutManager layoutManager = new LinearLayoutManager(MainActivity.this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemViewCacheSize(1000);

        currenciesViewModel = ViewModelProviders.of(MainActivity.this).get(CurrenciesViewModel.class);
        convertViewModel = ViewModelProviders.of(MainActivity.this).get(ConvertViewModel.class);
        allCurrenciesViewModel = ViewModelProviders.of(MainActivity.this).get(AllCurrenciesViewModel.class);


        // Update Data every 5 min
        Timer timer = new Timer();
        TimerTask hourlyTask = new TimerTask() {
            @Override
            public void run() {
                callAPIs();
            }
        };

        timer.schedule(hourlyTask, 0l, 1000 * 60 * 60 * 5);
    }

    private void callAPIs() {
        final CurrencyAdapter currencyAdapter = new CurrencyAdapter(MainActivity.this, listener);

        allCurrenciesViewModel.getAllCurencies().observe(this, new Observer<AllCurrencies>() {
            @Override
            public void onChanged(@Nullable AllCurrencies allCurrencies) {
                items = allCurrencies.getData();
            }
        });

        currenciesViewModel.currencyList.observe(MainActivity.this, pagedList -> {
            swipeRefreshLayout.setRefreshing(false);
            currencyAdapter.submitList(pagedList);
        });

        currenciesViewModel.networkState.observe(MainActivity.this, networkState -> {
            swipeRefreshLayout.setRefreshing(false);
            currencyAdapter.setNetworkState(networkState);
        });
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                recyclerView.setAdapter(currencyAdapter);
            }
        });

    }

    @OnClick(R.id.tv_compare)
    protected void onCompareClicked() {
        if (mCheckedItems.size() > 0) {
            Intent intent = new Intent(MainActivity.this, ChartActivity.class);
            String arrayAsString = new Gson().toJson(mCheckedItems);
            intent.putExtra("array", arrayAsString);
            startActivity(intent);
        }
    }

    @OnClick(R.id.tv_calc)
    protected void onCalculate() {
        if (!TextUtils.isEmpty(TV_Item.getText().toString()) && Edit_Amount.getText().toString() != "0" && currentItem != null) {
            convertViewModel.getValue(currentItem.getId()).observe(MainActivity.this, new Observer<CurrencyValue>() {
                @Override
                public void onChanged(@Nullable CurrencyValue currencyValue) {
                    double price = currencyValue.getData().getQuotes().getUSD().getPrice();
                    int amount = Integer.parseInt(Edit_Amount.getText().toString());

                    double result = price * amount;
                    TV_Result.setText("$" + String.valueOf(result));
                }
            });
        }
    }

    @OnClick(R.id.tv_item)
    protected void onItemClicked() {
        if (recyclerPopupWindow == null) {
            recyclerPopupWindow = new RecyclerPopupWindow(items);
            recyclerPopupWindow.showPopupWindow(MainActivity.this, TV_Item, TV_Item.getWidth() * 2, 500, false);
            recyclerPopupWindow.setCallBack(new RecyclerPopupWindow.CallBack() {
                @Override
                public void callback(AllCurrencies.Datum value) {
                    if (!"-1".equals(value)) {
                        currentItem = value;
                        TV_Item.setText(value.getName());
                    }
                    recyclerPopupWindow = null;
                }
            });
        }
    }

    @Override
    public void onRetryClick(View view, int position) {
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (unbinder != null) {
            unbinder.unbind();
            unbinder = null;
        }
    }
}
