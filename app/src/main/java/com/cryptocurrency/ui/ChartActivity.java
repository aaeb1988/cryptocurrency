package com.cryptocurrency.ui;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.cryptocurrency.R;
import com.cryptocurrency.model.POJO.CurrencyModel;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.ithebk.barchart.BarChart;
import me.ithebk.barchart.BarChartModel;

public class ChartActivity extends AppCompatActivity {

    @BindView(R.id.bar_chart_vertical)
    BarChart barChart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chart);

        ButterKnife.bind(this);

        String arrayAsString = getIntent().getExtras().getString("array");
        List<CurrencyModel.Datum> list = Arrays.asList(new Gson().fromJson(arrayAsString, CurrencyModel.Datum[].class));

        barChart.setOnBarClickListener(new BarChart.OnBarClickListener() {
            @Override
            public void onBarClick(BarChartModel barChartModel) {
                //Returns the bar you have clicked.
                removeBar(barChartModel);
            }
        });

        Collections.sort(list);
        addBar(list);
    }


    private void addBar(List<CurrencyModel.Datum> list) {
        barChart.setBarMaxValue((int) (list.get(0).getQuotes().getUSD().getPrice() / 1));
//        List<BarChartModel> barChartModelList = new ArrayList<>();

        for (CurrencyModel.Datum datum : list) {
            BarChartModel barChartModel = new BarChartModel();
            barChartModel.setBarValue((int) (datum.getQuotes().getUSD().getPrice() / 1));
            barChartModel.setBarColor(Color.parseColor("#9C27B0"));
            barChartModel.setBarTag(null); //You can set your own tag to bar model
            barChartModel.setBarText(datum.getSymbol());

            barChart.addBar(barChartModel);
        }


    }

    private void removeBar(BarChartModel barChartModel) {
        //To remove BarChartModel object from the list
        barChart.removeBar(barChartModel);
    }
}
