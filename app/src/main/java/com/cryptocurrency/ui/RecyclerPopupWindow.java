package com.cryptocurrency.ui;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.PopupWindow;

import com.cryptocurrency.R;
import com.cryptocurrency.model.POJO.AllCurrencies;
import com.cryptocurrency.model.RecyclerPopupWindowAdapter;

import java.util.List;

public class RecyclerPopupWindow extends PopupWindow implements  PopupWindow.OnDismissListener, RecyclerPopupWindowAdapter.OnItemClickListener {
    private List<AllCurrencies.Datum> items;
    private PopupWindow popupWindow;
    private RecyclerView recyclerView;
    private RecyclerPopupWindowAdapter recyclerPopupWindowAdapter;
    private CallBack mCallBack;
    private int position;
    private int prePosition;
    private boolean isClickItem;


    public RecyclerPopupWindow(List<AllCurrencies.Datum> items) {
        this.items = items;
//        for (int i = 1; i < items.size(); ++i) {
//            if (items.get(i).isActive()) {
//                prePosition = i;
//                break;
//            }
//        }
    }


    public void showPopupWindow(Context context, View anchor, int window_width, int window_height, boolean isUp) {
        View contentView = LayoutInflater.from(context).inflate(R.layout.popup_window, null);
        popupWindow = new PopupWindow(contentView, window_width, window_height, true);
        popupWindow.setBackgroundDrawable(new ColorDrawable(0x00000000));
        popupWindow.setOutsideTouchable(true);

        popupWindow.setOnDismissListener(this);
        recyclerView = (RecyclerView) contentView.findViewById(R.id.rv_function_wash_time);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerPopupWindowAdapter = new RecyclerPopupWindowAdapter(items);
        recyclerPopupWindowAdapter.setOnItemClickListener(this);
        recyclerView.setAdapter(recyclerPopupWindowAdapter);
        if (isUp) {
            popupWindow.setAnimationStyle(R.style.Popwindow_Anim_Up);
            popupWindow.showAsDropDown(anchor, 0, -(window_height + anchor.getHeight()));
        } else {
            popupWindow.setAnimationStyle(R.style.Popwindow_Anim_Down);
            popupWindow.showAsDropDown(anchor, 0, 0);
        }
    }

    public void setCallBack(CallBack callBack) {
        mCallBack = callBack;
    }

    @Override
    public void onItemClick(int pos) {
        position = pos;
        isClickItem = true;
        changePos(true);
    }

    private void changePos(boolean isCloseWindow) {
        if (position != prePosition && prePosition != 0) {
//            items.get(prePosition).setActive(false);
            recyclerPopupWindowAdapter.notifyItemChanged(prePosition);
        }
        if (position > 0) {
//            items.get(position).setActive(true);
            recyclerPopupWindowAdapter.notifyItemChanged(position);
        }
        if (isCloseWindow) {
            (new Handler()).postDelayed(new Runnable() {
                @Override
                public void run() {
                    mCallBack.callback(items.get(position));
                    destroyPopWindow();
                }
            }, 450);
        }
    }

    @Override
    public void onDismiss() {
        if (!isClickItem) {
            mCallBack.callback(null);
        }
    }


    private void destroyPopWindow() {
        if (popupWindow != null) {
            popupWindow.dismiss();
            popupWindow = null;
        }
    }

    public interface CallBack {
        void callback(AllCurrencies.Datum value);
    }
}
