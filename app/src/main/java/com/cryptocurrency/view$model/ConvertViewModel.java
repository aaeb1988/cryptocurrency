package com.cryptocurrency.view$model;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.cryptocurrency.model.POJO.AllCurrencies;
import com.cryptocurrency.model.POJO.CurrencyValue;
import com.cryptocurrency.model.remote.ConvertRepo;
import com.cryptocurrency.model.remote.CurrenciesRepo;


public class ConvertViewModel extends ViewModel {

    private LiveData<CurrencyValue> currencyValueLiveData;

    public ConvertViewModel() {
    }

    public LiveData<CurrencyValue> getValue(int id) {
        currencyValueLiveData = ConvertRepo.getInstance().getValue(id);
        return currencyValueLiveData;
    }

}
