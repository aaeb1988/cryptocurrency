package com.cryptocurrency.view$model;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;
import android.arch.paging.LivePagedListBuilder;
import android.arch.paging.PagedList;

import com.cryptocurrency.model.POJO.CurrencyModel;
import com.cryptocurrency.model.remote.paging.CurrencyDataSourceFactory;
import com.cryptocurrency.model.remote.paging.ItemKeyedCurrencyDataSource;
import com.cryptocurrency.util.NetworkState;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class CurrenciesViewModel extends ViewModel {

    public LiveData<PagedList<CurrencyModel.Datum>> currencyList;
    public LiveData<NetworkState> networkState;
    Executor executor;
    LiveData<ItemKeyedCurrencyDataSource> tDataSource;

    public CurrenciesViewModel() {
        executor = Executors.newFixedThreadPool(5);
        CurrencyDataSourceFactory currencyDataSourceFactory = new CurrencyDataSourceFactory(executor);

        tDataSource = currencyDataSourceFactory.getMutableLiveData();

        networkState = Transformations.switchMap(currencyDataSourceFactory.getMutableLiveData(), dataSource -> {
            return dataSource.getNetworkState();
        });

        PagedList.Config pagedListConfig =
                (new PagedList.Config.Builder()).setEnablePlaceholders(false)
                        .setInitialLoadSizeHint(10)
                        .setPageSize(10).build();

        currencyList = (new LivePagedListBuilder(currencyDataSourceFactory, pagedListConfig))
                .setFetchExecutor(executor)
                .build();
    }
}
