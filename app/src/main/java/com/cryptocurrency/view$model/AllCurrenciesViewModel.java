package com.cryptocurrency.view$model;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.cryptocurrency.model.POJO.AllCurrencies;
import com.cryptocurrency.model.remote.CurrenciesRepo;

public class AllCurrenciesViewModel extends ViewModel{
    private final LiveData<AllCurrencies> allCurrenciesLiveData;

    public AllCurrenciesViewModel() {
        allCurrenciesLiveData = CurrenciesRepo.getInstance().getAllCurrencies();
    }

    public LiveData<AllCurrencies> getAllCurencies() {
        return allCurrenciesLiveData;
    }
}
