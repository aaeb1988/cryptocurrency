package com.cryptocurrency.model.remote;

import android.database.Observable;

import com.cryptocurrency.model.POJO.AllCurrencies;
import com.cryptocurrency.model.POJO.CurrencyModel;
import com.cryptocurrency.model.POJO.CurrencyValue;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiService {

    @GET("?")
    Call<CurrencyModel> getCurrenciesList(@Query("start") int start, @Query("limit") int limit
            , @Query("sort") String sort, @Query("structure") String structure);

    @GET(".")
    Call<AllCurrencies> getAllCurrencies();

    @GET("{id}")
    Call<CurrencyValue> getValue(@Path("id") int id);
}
