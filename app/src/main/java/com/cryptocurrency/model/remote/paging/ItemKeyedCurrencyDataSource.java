package com.cryptocurrency.model.remote.paging;

import android.arch.lifecycle.MutableLiveData;
import android.arch.paging.ItemKeyedDataSource;
import android.support.annotation.NonNull;
import android.util.Log;

import com.cryptocurrency.model.POJO.CurrencyModel;
import com.cryptocurrency.model.remote.ApiService;
import com.cryptocurrency.model.remote.RetrofitClass;
import com.cryptocurrency.util.NetworkState;
import com.cryptocurrency.util.Status;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ItemKeyedCurrencyDataSource extends ItemKeyedDataSource<Integer, CurrencyModel.Datum> {
    public static final String TAG = "ItemCurrencyDataSource";
    ApiService apiService;
    LoadInitialParams<Integer> initialParams;
    LoadParams<Integer> afterParams;
    private MutableLiveData networkState;
    private MutableLiveData initialLoading;
    private Executor retryExecutor;

    public ItemKeyedCurrencyDataSource(Executor retryExecutor) {
        apiService = RetrofitClass.getApiService(RetrofitClass.BASE_URL);
        networkState = new MutableLiveData();
        initialLoading = new MutableLiveData();
        this.retryExecutor = retryExecutor;
    }


    public MutableLiveData getNetworkState() {
        return networkState;
    }

    public MutableLiveData getInitialLoading() {
        return initialLoading;
    }

    @Override
    public void loadInitial(@NonNull LoadInitialParams<Integer> params, @NonNull final LoadInitialCallback<CurrencyModel.Datum> callback) {
        Log.i(TAG, "Loading Rang " + 1 + " Count " + params.requestedLoadSize);
        initialParams = params;
        final List<CurrencyModel.Datum> currencies = new ArrayList();

        initialLoading.postValue(NetworkState.LOADING);
        networkState.postValue(NetworkState.LOADING);
        apiService.getCurrenciesList(1,params.requestedLoadSize,"id","array" ).enqueue(new Callback<CurrencyModel>() {
            @Override
            public void onResponse(Call<CurrencyModel> call, Response<CurrencyModel> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    currencies.addAll(response.body().getData());
                    callback.onResult(currencies);


                    initialLoading.postValue(NetworkState.LOADED);
                    networkState.postValue(NetworkState.LOADED);
                    initialParams = null;
                } else {
                    Log.e("API CALL", response.message());
                    initialLoading.postValue(new NetworkState(Status.FAILED, response.message()));
                    networkState.postValue(new NetworkState(Status.FAILED, response.message()));
                }
            }

            @Override
            public void onFailure(Call<CurrencyModel> call, Throwable t) {
                String errorMessage;
                errorMessage = t.getMessage();
                if (t == null) {
                    errorMessage = "unknown error";
                }
                networkState.postValue(new NetworkState(Status.FAILED, errorMessage));
            }
        });

    }

    @Override
    public void loadAfter(@NonNull LoadParams<Integer> params, @NonNull final LoadCallback<CurrencyModel.Datum> callback) {
        Log.i(TAG, "Loading Rang " + params.key + " Count " + params.requestedLoadSize);
        final List<CurrencyModel.Datum> currencies = new ArrayList();
        afterParams = params;

        networkState.postValue(NetworkState.LOADING);
        apiService.getCurrenciesList(params.key, params.requestedLoadSize,"id","array").enqueue(new Callback<CurrencyModel>() {
            @Override
            public void onResponse(Call<CurrencyModel> call, Response<CurrencyModel> response) {
                if (response.isSuccessful()) {
                    currencies.addAll(response.body().getData());
                    callback.onResult(currencies);
                    networkState.postValue(NetworkState.LOADED);
                    afterParams = null;
                } else {
                    networkState.postValue(new NetworkState(Status.FAILED, response.message()));
                    Log.e("API CALL", response.message());
                }
            }

            @Override
            public void onFailure(Call<CurrencyModel> call, Throwable t) {
                String errorMessage;
                errorMessage = t.getMessage();
                if (t == null) {
                    errorMessage = "unknown error";
                }
                networkState.postValue(new NetworkState(Status.FAILED, errorMessage));
            }
        });

    }

    @Override
    public void loadBefore(@NonNull LoadParams<Integer> params, @NonNull LoadCallback<CurrencyModel.Datum> callback) {

    }

    @NonNull
    @Override
    public Integer getKey(@NonNull CurrencyModel.Datum item) {
        return item.getId();
    }

}
