package com.cryptocurrency.model.remote;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;

import com.cryptocurrency.model.POJO.AllCurrencies;
import com.cryptocurrency.model.POJO.CurrencyValue;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ConvertRepo {
    private static ConvertRepo convertRepo;

    public static ConvertRepo getInstance(){
        if(convertRepo==null){
            convertRepo=new ConvertRepo();
        }
        return convertRepo;
    }

    public LiveData<CurrencyValue> getValue(int id) {
        final MutableLiveData<CurrencyValue> data = new MutableLiveData<>();
        RetrofitClass.getApiService(RetrofitClass.BASE_URL)
                .getValue(id).enqueue(new Callback<CurrencyValue>() {
            @Override
            public void onResponse(Call<CurrencyValue> call, Response<CurrencyValue> response) {
                data.setValue(response.body());
            }

            @Override
            public void onFailure(Call<CurrencyValue> call, Throwable t) {

            }
        });
        return data;
    }
}
