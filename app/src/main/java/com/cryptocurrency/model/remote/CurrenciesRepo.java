package com.cryptocurrency.model.remote;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.database.Observable;

import com.cryptocurrency.model.POJO.AllCurrencies;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CurrenciesRepo {
    private static CurrenciesRepo currenciesRepo;

    public static CurrenciesRepo getInstance(){
        if(currenciesRepo==null){
            currenciesRepo=new CurrenciesRepo();
        }
        return currenciesRepo;
    }

    public LiveData<AllCurrencies> getAllCurrencies() {
        final MutableLiveData<AllCurrencies> data = new MutableLiveData<>();
        RetrofitClass.getApiService(RetrofitClass.BASE_URL2)
                .getAllCurrencies().enqueue(new Callback<AllCurrencies>() {
            @Override
            public void onResponse(Call<AllCurrencies> call, Response<AllCurrencies> response) {
                data.setValue(response.body());
            }

            @Override
            public void onFailure(Call<AllCurrencies> call, Throwable t) {

            }
        });
        return data;
    }
}
