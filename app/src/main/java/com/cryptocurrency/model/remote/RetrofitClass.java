package com.cryptocurrency.model.remote;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClass {
    public static final String BASE_URL = "https://api.coinmarketcap.com/v2/ticker/";
    public static final String BASE_URL2 = "https://api.coinmarketcap.com/v2/listings/";

    private static Retrofit getInstance(String url) {

        Gson gson = new GsonBuilder().setLenient().create();

        return new Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create(gson)).build();
    }

    public static ApiService getApiService(String url) {
        return getInstance(url).create(ApiService.class);
    }
}
