package com.cryptocurrency.model.remote.paging;

import android.arch.lifecycle.MutableLiveData;
import android.arch.paging.DataSource;

import java.util.concurrent.Executor;

public class CurrencyDataSourceFactory extends DataSource.Factory {

    MutableLiveData<ItemKeyedCurrencyDataSource> mutableLiveData;
    ItemKeyedCurrencyDataSource itemKeyedCurrencyDataSource;
    Executor executor;

    public CurrencyDataSourceFactory(Executor executor) {
        this.mutableLiveData = new MutableLiveData<>();
        this.executor = executor;
    }


    @Override
    public DataSource create() {
        itemKeyedCurrencyDataSource = new ItemKeyedCurrencyDataSource(executor);
        mutableLiveData.postValue(itemKeyedCurrencyDataSource);
        return itemKeyedCurrencyDataSource;
    }

    public MutableLiveData<ItemKeyedCurrencyDataSource> getMutableLiveData() {
        return mutableLiveData;
    }
}
