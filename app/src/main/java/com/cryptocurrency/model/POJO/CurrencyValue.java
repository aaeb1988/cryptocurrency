package com.cryptocurrency.model.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CurrencyValue {

    @SerializedName("data")
    @Expose
    private CurrencyModel.Datum data;
    @SerializedName("metadata")
    @Expose
    private CurrencyModel.Metadata metadata;

    public CurrencyModel.Datum getData() {
        return data;
    }

    public void setData(CurrencyModel.Datum data) {
        this.data = data;
    }

    public CurrencyModel.Metadata getMetadata() {
        return metadata;
    }

    public void setMetadata(CurrencyModel.Metadata metadata) {
        this.metadata = metadata;
    }


}




