package com.cryptocurrency.model.POJO;

import android.support.annotation.NonNull;

import java.util.Comparator;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import android.support.v7.util.DiffUtil;

public class CurrencyModel {

    @SerializedName("data")
    @Expose
    private List<Datum> data = null;
    @SerializedName("metadata")
    @Expose
    private Metadata metadata;

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public Metadata getMetadata() {
        return metadata;
    }

    public void setMetadata(Metadata metadata) {
        this.metadata = metadata;
    }


    public static class Datum implements Comparable {

        public static DiffUtil.ItemCallback<Datum> CALLBACK = new DiffUtil.ItemCallback<Datum>() {
            @Override
            public boolean areItemsTheSame(Datum oldItem, Datum newItem) {
                return oldItem.getId() == newItem.getId();
            }

            @Override
            public boolean areContentsTheSame(Datum oldItem, Datum newItem) {
                return oldItem.equals(newItem);
            }
        };


        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getSymbol() {
            return symbol;
        }

        public void setSymbol(String symbol) {
            this.symbol = symbol;
        }

        public String getWebsiteSlug() {
            return websiteSlug;
        }

        public void setWebsiteSlug(String websiteSlug) {
            this.websiteSlug = websiteSlug;
        }

        public int getRank() {
            return rank;
        }

        public void setRank(int rank) {
            this.rank = rank;
        }

        public long getCirculatingSupply() {
            return circulatingSupply;
        }

        public void setCirculatingSupply(long circulatingSupply) {
            this.circulatingSupply = circulatingSupply;
        }

        public long getTotalSupply() {
            return totalSupply;
        }

        public void setTotalSupply(long totalSupply) {
            this.totalSupply = totalSupply;
        }

        public String getMaxSupply() {
            return maxSupply;
        }

        public void setMaxSupply(String maxSupply) {
            this.maxSupply = maxSupply;
        }

        public Quotes getQuotes() {
            return quotes;
        }

        public void setQuotes(Quotes quotes) {
            this.quotes = quotes;
        }

        public long getLastUpdated() {
            return lastUpdated;
        }

        public void setLastUpdated(long lastUpdated) {
            this.lastUpdated = lastUpdated;
        }

        @SerializedName("id")
        @Expose
        private int id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("symbol")
        @Expose
        private String symbol;
        @SerializedName("website_slug")
        @Expose
        private String websiteSlug;
        @SerializedName("rank")
        @Expose
        private int rank;
        @SerializedName("circulating_supply")
        @Expose
        private long circulatingSupply;
        @SerializedName("total_supply")
        @Expose
        private long totalSupply;
        @SerializedName("max_supply")
        @Expose
        private String maxSupply;
        @SerializedName("quotes")
        @Expose
        private Quotes quotes;
        @SerializedName("last_updated")
        @Expose
        private long lastUpdated;

        @Override
        public int compareTo(@NonNull Object o) {
            double price = ((Datum) o).getQuotes().getUSD().getPrice();

            if(this.getQuotes().getUSD().getPrice() > price){
                return 0;
            }
            else if(this.getQuotes().getUSD().getPrice() < price) {
                return 1;
            }
            else{
                return -1;
            }

        }
    }

    public class Metadata {

        @SerializedName("timestamp")
        @Expose
        private Integer timestamp;
        @SerializedName("num_cryptocurrencies")
        @Expose
        private Integer numCryptocurrencies;
        @SerializedName("error")
        @Expose
        private Object error;

        public Integer getTimestamp() {
            return timestamp;
        }

        public void setTimestamp(Integer timestamp) {
            this.timestamp = timestamp;
        }

        public Integer getNumCryptocurrencies() {
            return numCryptocurrencies;
        }

        public void setNumCryptocurrencies(Integer numCryptocurrencies) {
            this.numCryptocurrencies = numCryptocurrencies;
        }

        public Object getError() {
            return error;
        }

        public void setError(Object error) {
            this.error = error;
        }

    }

    public class Quotes {

        @SerializedName("USD")
        @Expose
        private USD uSD;

        public USD getUSD() {
            return uSD;
        }

        public void setUSD(USD uSD) {
            this.uSD = uSD;
        }

    }

    public class USD {

        public double getPrice() {
            return price;
        }

        public void setPrice(double price) {
            this.price = price;
        }

        public double getVolume24h() {
            return volume24h;
        }

        public void setVolume24h(double volume24h) {
            this.volume24h = volume24h;
        }

        public long getMarketCap() {
            return marketCap;
        }

        public void setMarketCap(long marketCap) {
            this.marketCap = marketCap;
        }

        public float getPercentChange1h() {
            return percentChange1h;
        }

        public void setPercentChange1h(float percentChange1h) {
            this.percentChange1h = percentChange1h;
        }

        public float getPercentChange24h() {
            return percentChange24h;
        }

        public void setPercentChange24h(float percentChange24h) {
            this.percentChange24h = percentChange24h;
        }

        public float getPercentChange7d() {
            return percentChange7d;
        }

        public void setPercentChange7d(float percentChange7d) {
            this.percentChange7d = percentChange7d;
        }

        @SerializedName("price")
        @Expose
        private double price;
        @SerializedName("volume_24h")
        @Expose
        private double volume24h;
        @SerializedName("market_cap")
        @Expose
        private long marketCap;
        @SerializedName("percent_change_1h")
        @Expose
        private float percentChange1h;
        @SerializedName("percent_change_24h")
        @Expose
        private float percentChange24h;
        @SerializedName("percent_change_7d")
        @Expose
        private float percentChange7d;


    }
}

