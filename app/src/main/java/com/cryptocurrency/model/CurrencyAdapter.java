package com.cryptocurrency.model;

import android.arch.paging.PagedList;
import android.arch.paging.PagedListAdapter;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.cryptocurrency.R;
import com.cryptocurrency.model.POJO.CurrencyModel;
import com.cryptocurrency.util.NetworkState;
import com.cryptocurrency.util.Status;
import com.hanks.library.AnimateCheckBox;

public class CurrencyAdapter extends PagedListAdapter<CurrencyModel.Datum, RecyclerView.ViewHolder> {

    private static final String TAG = "CurrencyAdapter";
    private NetworkState networkState;
    private ListItemClickListener itemClickListener;
    public CheckBoxListener mListener;

    public CurrencyAdapter(ListItemClickListener itemClickListener, CheckBoxListener listener) {
        super(CurrencyModel.Datum.CALLBACK);
        this.itemClickListener = itemClickListener;
        this.mListener = listener;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view;

        if (viewType == R.layout.currency_row) {
            view = layoutInflater.inflate(R.layout.currency_row, parent, false);
            return new CurrencyItemViewHolder(view);
        } else if (viewType == R.layout.network_state_row) {
            view = layoutInflater.inflate(R.layout.network_state_row, parent, false);
            return new NetworkStateItemViewHolder(view, itemClickListener);
        } else {
            throw new IllegalArgumentException("unknown view type");
        }
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (getItemViewType(position)) {
            case R.layout.currency_row:
                ((CurrencyItemViewHolder) holder).bindTo(getItem(position));
                break;
            case R.layout.network_state_row:
                ((NetworkStateItemViewHolder) holder).bindView(networkState);
                break;
        }
    }

    private boolean hasExtraRow() {
        if (networkState != null && networkState != NetworkState.LOADED) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (hasExtraRow() && position == getItemCount() - 1) {
            return R.layout.network_state_row;
        } else {
            return R.layout.currency_row;
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void setNetworkState(NetworkState newNetworkState) {
        NetworkState previousState = this.networkState;
        boolean previousExtraRow = hasExtraRow();
        this.networkState = newNetworkState;
        boolean newExtraRow = hasExtraRow();
        if (previousExtraRow != newExtraRow) {
            if (previousExtraRow) {
                notifyItemRemoved(getItemCount());
            } else {
                notifyItemInserted(getItemCount());
            }
        } else if (newExtraRow && previousState != newNetworkState) {
            notifyItemChanged(getItemCount() - 1);
        }
    }

     class CurrencyItemViewHolder extends RecyclerView.ViewHolder {
        TextView name, rate;
        AnimateCheckBox checkBox;

        public CurrencyItemViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.currency_title_tv);
            rate = itemView.findViewById(R.id.currency_rate_tv);
            checkBox = itemView.findViewById(R.id.checkbox);
            checkBox.setOnCheckedChangeListener(new AnimateCheckBox.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(View buttonView, boolean isChecked) {
                    mListener.onItemChecked(getItem(getAdapterPosition()),isChecked);
                }
            });
        }


        public void bindTo(CurrencyModel.Datum item) {
            name.setText(item.getName());
            rate.setText("$" + String.valueOf(item.getQuotes().getUSD().getPrice()));
        }
    }

    static class NetworkStateItemViewHolder extends RecyclerView.ViewHolder {

        private final ProgressBar progressBar;
        private final TextView errorMsg;
        private Button button;

        public NetworkStateItemViewHolder(View itemView, ListItemClickListener listItemClickListener) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progress_bar);
            errorMsg = itemView.findViewById(R.id.error_msg);
            button = itemView.findViewById(R.id.retry_button);

            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listItemClickListener.onRetryClick(view, getAdapterPosition());
                }
            });
        }


        public void bindView(NetworkState networkState) {
            if (networkState != null && networkState.getStatus() == Status.RUNNING) {
                progressBar.setVisibility(View.VISIBLE);
            } else {
                progressBar.setVisibility(View.GONE);
            }

            if (networkState != null && networkState.getStatus() == Status.FAILED) {
                errorMsg.setVisibility(View.VISIBLE);
                errorMsg.setText(networkState.getMsg());
            } else {
                errorMsg.setVisibility(View.GONE);
            }
        }
    }
}