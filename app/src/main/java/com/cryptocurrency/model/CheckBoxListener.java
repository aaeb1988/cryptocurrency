package com.cryptocurrency.model;

import android.view.View;

import com.cryptocurrency.model.POJO.CurrencyModel;

public interface CheckBoxListener {
    void onItemChecked(CurrencyModel.Datum datum,boolean isChecked);
}
