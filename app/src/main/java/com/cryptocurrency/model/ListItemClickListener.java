package com.cryptocurrency.model;

import android.view.View;

public interface ListItemClickListener {
    void onRetryClick(View view, int adapterPosition);
}
